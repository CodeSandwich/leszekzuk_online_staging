# Czerwona linia

![Horror vacui - dualizm pustki](inputs/grafiki/Czerwona linia1.Horror vacui - dualizm pustki.jpg)

---

![Drzewa życia - multiverse](inputs/grafiki/Czerwona linia2.Drzewa życia - multiverse.jpg)

---

![Jin jang - dialektyka przeciwieństw](inputs/grafiki/Czerwona linia3.Jin jang - dialektyka przeciwieństw.jpg)

---

![Scientia czyli porządek z chaosu](inputs/grafiki/Czerwona linia4.Scientia czyli porządek z chaosu.jpg)

---

![Quo vadis?](inputs/grafiki/Czerwona linia5.Quo vadis.jpg)

---

![Deus ex machina](inputs/grafiki/Czerwona linia6.Deus ex machina.jpg)

---

![Lewiatan, feniks i nowy początek](inputs/grafiki/Czerwona linia7.Lewiatan, feniks i nowy początek.jpg)


# Dawno temu

![](inputs/grafiki/Dawno temu1.jpg)

---

![](inputs/grafiki/Dawno temu2.jpg)

---

![](inputs/grafiki/Dawno temu3.jpg)

---

![](inputs/grafiki/Dawno temu4.jpg)

---

![](inputs/grafiki/Dawno temu5.jpg)

---

![](inputs/grafiki/Dawno temu6.jpg)

# Dwoistość

![](inputs/grafiki/Dwoistość1.jpg)

---

![](inputs/grafiki/Dwoistość2.jpg)

---

![](inputs/grafiki/Dwoistość3.png)

---

![](inputs/grafiki/Dwoistość4.jpg)

---

![](inputs/grafiki/Dwoistość5.jpg)

---

![](inputs/grafiki/Dwoistość6.jpg)

---

![](inputs/grafiki/Dwoistość7.jpg)

---

![](inputs/grafiki/Dwoistość8.jpg)

# Futurologia

![](inputs/grafiki/Futurologia1.jpg)

---

![](inputs/grafiki/Futurologia2.jpg)

---

![](inputs/grafiki/Futurologia3.jpg)

# Homo

![Staję się](inputs/grafiki/Homo1.Staję się.jpg)

---

![Wobec wieczności](inputs/grafiki/Homo2.Wobec wieczności.jpg)

---

![Wola](inputs/grafiki/Homo3.Wola.jpg)

---

![Honor](inputs/grafiki/Homo4.Honor.jpg)

---

![Trybalizm](inputs/grafiki/Homo5.Trybalizm.jpg)

---

![Tradycja](inputs/grafiki/Homo6.Tradycja.jpg)

---

![Showbusiness](inputs/grafiki/Homo7.Showbusiness.jpg)

---

![Koncert](inputs/grafiki/Homo8.Koncert.jpg)

---

![Chwila refleksji](inputs/grafiki/Homo9.Chwila refleksji.jpg)

---

![Transcendencja](inputs/grafiki/Homo10.Transcendencja.jpg)

---

![Próby](inputs/grafiki/Homo11.Próby.jpg)

# Jelenia Góra

![Kotlina](inputs/grafiki/Jelenia Góra1.Kotlina.jpg)

---

![Liczyrzepa](inputs/grafiki/Jelenia Góra2.Liczyrzepa.jpg)

---

![Grodzisko](inputs/grafiki/Jelenia Góra3.Grodzisko.png)

---

![Barok](inputs/grafiki/Jelenia Góra4.Barok.jpg)

---

![Noc na cmentarzu](inputs/grafiki/Jelenia Góra5.Noc na cmentarzu.jpg)

---

![Gotyk](inputs/grafiki/Jelenia Góra6.Gotyk.jpg)

---

![Baszta](inputs/grafiki/Jelenia Góra7.Baszta.jpg)

---

![Teatr](inputs/grafiki/Jelenia Góra8.Teatr.jpg)

---

![Był tramwaj](inputs/grafiki/Jelenia Góra9.Był tramwaj.jpg)

---

![Linia murów](inputs/grafiki/Jelenia Góra10.Linia murów.jpg)

# Kobieta

![](inputs/grafiki/Kobieta1.jpg)

---

![](inputs/grafiki/Kobieta2.jpg)

---

![](inputs/grafiki/Kobieta3.jpg)

---

![](inputs/grafiki/Kobieta4.jpg)

---

![](inputs/grafiki/Kobieta5l.jpg)

---

![](inputs/grafiki/Kobieta6.jpg)

---

![](inputs/grafiki/Kobieta7.jpg)

---

![](inputs/grafiki/Kobieta8.jpg)

---

![](inputs/grafiki/Kobieta9.jpg)

---

![](inputs/grafiki/Kobieta10.jpg)

---

![](inputs/grafiki/Kobieta11.jpg)

# Medicina

![](inputs/grafiki/medicina1.jpg)

---

![](inputs/grafiki/medicina2.jpg)

---

![](inputs/grafiki/medicina3.jpg)

# Podróże

![](inputs/grafiki/Podróze1.jpg)

---

![](inputs/grafiki/Podróże2.jpg)

---

![](inputs/grafiki/Podróże3.jpg)

---

![](inputs/grafiki/Podróże4.jpg)

---

![](inputs/grafiki/Podróże5.jpg)

---

![](inputs/grafiki/Podróże6.jpg)

---

![](inputs/grafiki/Podróże7.jpg)

---

![](inputs/grafiki/Podróże8.jpg)

# Portrety

![](inputs/grafiki/Portrety1.jpg)

---

![](inputs/grafiki/Portrety2.jpg)

---

![](inputs/grafiki/Portrety3.jpg)

---

![](inputs/grafiki/Portrety4.jpg)

---

![](inputs/grafiki/Portrety5.jpg)

---

![](inputs/grafiki/Portrety7.jpg)

---

![](inputs/grafiki/Portrety8.jpg)

# Religia

![](inputs/grafiki/Religia1.png)

---

![](inputs/grafiki/Religia2.jpg)

---

![](inputs/grafiki/Religia3.jpg)

---

![](inputs/grafiki/Religia4.jpg)

---

![](inputs/grafiki/Religia5.jpg)

---

![](inputs/grafiki/Religia6.jpg)

---

![](inputs/grafiki/Religia7.jpg)

---

![](inputs/grafiki/Religia8.jpg)

# Universum

![Ogród wiedzy](inputs/grafiki/Universum1.Ogród Wiedzy.jpg)

---

![Kosmos człowieka](inputs/grafiki/Universum2.Kosmos człowieka.jpg)

---

![Tunel](inputs/grafiki/Universum3.Tunel.jpg)

---

![Woda żywa](inputs/grafiki/Universum4.Woda żywa.jpg)

# Zwierzęta

![](inputs/grafiki/Zwierzęta1.jpg)

---

![](inputs/grafiki/Zwierzęta2.jpg)

---

![](inputs/grafiki/Zwierzęta3.jpg)

---

![](inputs/grafiki/Zwierzęta4.jpg)

---

![](inputs/grafiki/Zwierzęta5.jpg)

---

![](inputs/grafiki/Zwierzęta6.jpg)

---

![](inputs/grafiki/Zwierzęta7.jpg)

---

![](inputs/grafiki/Zwierzęta8.jpg)

---

![](inputs/grafiki/Zwierzęta9.jpg)

---

![](inputs/grafiki/Zwierzęta11.jpg)

---

![](inputs/grafiki/Zwierzęta12.jpg)

---

![](inputs/grafiki/Zwierzęta13.jpg)

---

![](inputs/grafiki/Zwierzęta14.jpg)

---

![](inputs/grafiki/Zwierzęta16.jpg)

---

![](inputs/grafiki/Zwierzęta17.jpg)

---

![](inputs/grafiki/Zwierzęta18.jpg)

---

![](inputs/grafiki/Zwierzęta19.jpg)

---

![](inputs/grafiki/Zwierzęta20.jpg)
