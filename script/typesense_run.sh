#!/bin/sh

# Run the Typesense server. It can be visited under http://localhost:8108.

podman run -v "$(pwd)/typesense_data:/typesense_data:z" -p 8108:8108 --network host \
    docker.io/typesense/typesense:0.26.0.rc41 --api-key secret --data-dir /typesense_data
