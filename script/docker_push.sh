#!/bin/sh

# Publishes to docker hub the docker image previously built in docker_build.sh

set -e
. script/helpers/docker_tag.sh
podman login docker.io
podman push "$docker_tag"
podman logout docker.io
