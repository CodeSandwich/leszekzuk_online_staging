#!/bin/sh

# Builds the docker image and stores it locally
# Remember to first update script/helpers/docker_tag.sh and .gitlab-ci.yml build image name

set -e
. script/helpers/docker_tag.sh
podman build -f script/helpers/Dockerfile -t "$docker_tag" .
