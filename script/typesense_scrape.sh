#!/bin/sh

# Scrape the website and index it on a Typesense server using Typesense docsearch scraper.
# Params: <SCRAPE_URL (default: http://localhost)> <TYPESENSE_PROTOCOL (default: http)>
# <TYPESENSE_HOST (default: localhost)> <TYPESENSE_PORT (default: 8108)> <TYPESENSE_PATH (optional)>
# Env variables: TYPESENSE_API_KEY (default: secret)
# Examples:
# script/typesense_scrape.sh
# script/typesense_scrape.sh https://codesandwich.gitlab.io/leszekzuk_online_staging/ https leszekzuk-online-staging-typesense.fly.dev 443
# script/typesense_scrape.sh https://leszekzuk.online/ https leszekzuk-online-typesense.fly.dev 443

export TYPESENSE_API_KEY="${TYPESENSE_API_KEY:-secret}"
SCRAPE_URL="${1:-http://localhost}"
export TYPESENSE_PROTOCOL="${2:-http}"
export TYPESENSE_HOST="${3:-localhost}"
export TYPESENSE_PORT="${4:-8108}"
export TYPESENSE_PATH="$5"

export CONFIG='{
        "index_name": "leszekzuk-online",
        "start_urls": [ "'"$SCRAPE_URL"'" ],
        "selectors": {
            "default":{
                "lvl0": {
                    "selector": "//meta[@property=\"og:site_name\"]/@content",
                    "type": "xpath",
                    "global": true
                },
                "lvl1": "h1",
                "lvl2": "h2",
                "lvl3": "h3",
                "lvl4": "h4",
                "lvl5": "h5",
                "lvl6": "div.paragraph-anchor",
                "content": "div.paragraph"
            }
        },
        "selectors_exclude": [ "footer" ]
    }'
podman run --network host \
    -e TYPESENSE_API_KEY \
    -e CONFIG \
    -e TYPESENSE_PROTOCOL \
    -e TYPESENSE_HOST \
    -e TYPESENSE_PORT \
    -e TYPESENSE_PATH \
    docker.io/typesense/docsearch-scraper:0.9.1

# Ensure that the `search` API key exists
podman run --network host docker.io/curlimages/curl:8.4.0 \
    -H "X-TYPESENSE-API-KEY: $TYPESENSE_API_KEY" \
    --json '{
        "description":"Search-only",
        "value": "search",
        "actions": ["documents:search"],
        "collections": ["*"]
    }' \
    "$TYPESENSE_PROTOCOL://$TYPESENSE_HOST:$TYPESENSE_PORT/$TYPESENSE_PATH/keys"
echo
