#!/bin/sh

# Create jekyll projects based on documents in the project.
# Preconfigured for local development and being served using script/site_serve.sh.
# Params: <BASEURL>

set -e

print_log_separator() {
    echo
    echo "--------------------------------------------------"
    echo "$1"
    echo "--------------------------------------------------"
    echo
}

export BASEURL="$1"

print_log_separator "Converting books"
podman run \
    -e URL="http://localhost" \
    -e BASEURL \
    -e REPO_URL="http://example.com/repo" \
    -e TYPESENSE_URL="http://localhost:8108" \
    -e TEST=1 \
    -i -v "$(pwd)/:/volume:z" -w /volume \
    --entrypoint pandoc-lua \
    docker.io/pandoc/minimal:3.5.0-static \
    script/helpers/convert_books.lua

print_log_separator "Converting images"
podman run \
    -i -v "$(pwd)/:/volume:z" -w /volume \
    --entrypoint sh \
    docker.io/dpokidov/imagemagick:7.1.1-41-bullseye \
    script/helpers/convert_images.sh

print_log_separator "Building site"
. script/helpers/docker_tag.sh
podman run \
    -e ALLOW_ROBOTS \
    -i -v "$(pwd)/:/volume:z" -w /volume \
    "$docker_tag" \
    sh script/helpers/build_site.sh

if [ -d public ] && [ -n "$(ls public)" ]
then
    mkdir -p public_copy
    mv public/* public_copy
    mkdir -p "public/$BASEURL"
    mv public_copy/* "public/$BASEURL"
    rm -r public_copy
fi

print_log_separator "Success!"
