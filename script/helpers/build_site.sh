# Env args:
# - ALLOW_ROBOTS: optional, set to 1 to enable, default is disabled

set -e

TEMP_DIR="$(pwd)/temp"
OUT_DIR="public"
mkdir -p "$OUT_DIR"
rm -rf "$OUT_DIR"/*
# Build the Jekyll projects
for BOOK in "$TEMP_DIR"/books/*; do
    echo "Building $BOOK"
    cp -r template/* "$BOOK"
    TEMP_OUT="$TEMP_DIR/temp_out"
    (
        cd "$BOOK"
        pwd
        RUBYOPT="-W0" bundle exec jekyll build -d "$TEMP_OUT"
    )
    cp -r "$TEMP_OUT"/* "$OUT_DIR"
    rm -rf "$TEMP_OUT"
done
rm -rf "$TEMP_DIR"
# Create robots.txt
if [ "$ALLOW_ROBOTS" != "1" ]; then
    ROBOTS_TXT="$OUT_DIR/robots.txt"
    echo "User-agent: *" >> "$ROBOTS_TXT"
    echo "Disallow: *" >> "$ROBOTS_TXT"
fi
# Copy the minibar assets
cp -r /assets "$OUT_DIR"
# Remove the unused assets
assets="$OUT_DIR/assets"
rm "$assets/css/just-the-docs-default.css.map"
rm "$assets/css/just-the-docs-light.css"
rm "$assets/css/just-the-docs-light.css.map"
rm "$assets/css/just-the-docs-dark.css"
rm "$assets/css/just-the-docs-dark.css.map"
rm "$assets/js/search-data.json"
rm "$assets/js/vendor" -rf
