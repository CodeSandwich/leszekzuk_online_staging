local filters = require("script.helpers.convert_books.filters")
local book = require("script.helpers.convert_books.book")

local remove_image_titles = {}

function remove_image_titles.Image(image)
    image.title = ""
    return image
end

function split(doc)
    local parts = pandoc.List()
    for _, block in ipairs(doc.blocks) do
        if(block.t == "Header" and block.level == 1) then
            -- The header opening the part isn't included in the part
            parts:insert(pandoc.Pandoc{})
        -- Do not include blocks from before the first part starts
        elseif(#parts > 0) then
            if(block.t == "Header") then
                block.level = block.level - 1
            end
            parts:at(-1).blocks:insert(block)
        end
    end
    return parts
end

return function()
    doc = book.read_doc("000HOMO.odt")
        :walk(filters.remove_block_quotes)
        :walk(filters.remove_anchors)
        :walk(filters.convert_fake_lists_to_lists)
        :walk(remove_image_titles)
    for idx, part in ipairs(split(doc)) do
        local url = "czlowiek-"..idx
        local subtitle = ({
            "człowiek",
            "kultura",
            "historia społeczna i polityczna",
            "procesy unifikacji i globalizacji"
        })[idx]
        print("Converting part "..idx)
        title = "Człowiek kultura historia część "..idx..": "..subtitle
        local description = ({
            "Człowiek jako organizm powstały w określonych warunkach w wyniku długiej ewolucji. "
                .."Świadoma osoba z jej emocjami, intelektem i wolą oraz istota społeczna.",
            "Powstanie i historyczny rozwój poszczególnych elementów "
                .."ludzkiej kultury w różnych ejonach świata. "
                .."Od koncepcji znaku poprzez język i pismo po sport i rozrywki, rozmaite formy "
                .."sztuki, naukę oraz religie, filozofie i związane z nimi systemy etyczne.",
            "Kronika dziejów plemion,ludów i państw całego świata. Od Afryki przez Bliski i "
                .."Daleki Wschód, Europę, Australię i wyspy Oceanii aż po Nowy Świat.",
            "Unifikacji kultur i kształtowanie globalnej wspólnoty ludzi. "
                .."Zarówno na poziomie ekonomicznym i politycznym jak też kulturowym, "
                .."zwłaszcza od epoki wielkich odkryć geograficznych."
        })[idx]
        book.write(part, url, title, description)
    end
end
