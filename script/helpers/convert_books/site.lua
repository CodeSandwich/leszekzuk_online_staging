local book = require("script.helpers.convert_books.book")

local insert_book_links = {}

function insert_book_links.Para(para)
    if(pandoc.utils.stringify(para) == "@@BOOK LINKS@@") then
        local blocks = pandoc.Blocks({})
        for _, info in ipairs(book.read_book_infos()) do
            blocks:insert(pandoc.Header(4, info.title))
            blocks:insert(pandoc.Para(info.description))
            blocks:insert(pandoc.Para({pandoc.Link("Czytaj online", info.url)}))
        end
        return blocks
    end
end

function create_grafiki_chapters()
    local doc = book.read_doc("grafiki.md")
    local chapters = book.create_chapters(doc)
    chapters[1].level = 0
    local title = "Grafiki"
    chapters[1].title = title
    book.add_book_navigation(chapters, title, "")
    for _, chapter in ipairs(chapters) do
        chapter.url = "grafiki/"..chapter.url
    end
    return chapters
end

function insert_grafiki_chapters(chapters)
    local _, idx = chapters:find_if(function(chapter) return chapter.title == "@@GRAFIKI@@" end)
    for i, chapter in ipairs(create_grafiki_chapters()) do
        chapters:insert(idx + i, chapter)
    end
    chapters:remove(idx)
end

function insert_404_page(chapters)
    local title = "Błąd 404"
    local blocks = pandoc.Blocks({ pandoc.Header(1, title), pandoc.Para("Strona nie istnieje") })
    chapters:insert({ doc = pandoc.Pandoc(blocks), title = title, url = "404", nav_exclude = true })
end

return function()
    doc = book.read_doc("site.md")
        :walk(insert_book_links)
    local chapters = book.create_chapters(doc)
    -- Remove the content from before the first chapter
    chapters:remove(1)
    chapters[1].url = ""
    insert_grafiki_chapters(chapters)
    insert_404_page(chapters)
    book.write_chapters(chapters, "", "Leszek Żuk",
        "Zajmuję się filozofią, zwłaszcza filozofią nauki, historią, religioznawstwem, etyką "
        .."ze szczególnym uwzględnieniem etyki medycyny oraz naukami przyrodniczymi specjalizując "
        .."się w paleontologii. Poza tym piszę i rysuję. Zazwyczaj sam ilustruję swoje teksty.")
end
