local function temp_path(...)
    return pandoc.path.join({"temp", ...})
end

local function out_path(url, ...)
    if(url == "") then
        url = "_"
    end
    return temp_path("books", url, ...)
end

local clean_up_headers = {}

function clean_up_headers.Header(header)
    local content = pandoc.utils.stringify(header)
    -- doesn't contain non-space characters
    if(not string.find(content, "%S")) then
        return {}
    end
    header.content = pandoc.Inlines(content)
    return header
end

local function unique_id(used_ids, id, always_add_count, suffix)
    id = pandoc.text.lower(id)
    id = string.gsub(id, "ą", "a")
    id = string.gsub(id, "ć", "c")
    id = string.gsub(id, "ę", "e")
    id = string.gsub(id, "ł", "l")
    id = string.gsub(id, "ń", "n")
    id = string.gsub(id, "ó", "o")
    id = string.gsub(id, "ś", "s")
    id = string.gsub(id, "ź", "z")
    id = string.gsub(id, "ż", "z")
    id = string.gsub(id, "[^a-z0-9]", "-")
    id = string.gsub(id, "-+", "-")
    id = string.gsub(id, "^-", "")
    id = string.sub(id, 1, 80)
    id = string.gsub(id, "-$", "")
    if(suffix) then id = id.."-"..suffix end
    local id_root = id
    local collisions = 0
    while(used_ids[id] or always_add_count) do
        always_add_count = false
        collisions = collisions + 1
        id = id_root.."-"..collisions
    end
    used_ids[id] = true
    return id
end

local function split_chapters(doc)
    local split_level = 3
    local curr_chapter_doc = pandoc.Pandoc{}
    local chapters = pandoc.List({{
        doc = curr_chapter_doc,
        level = split_level,
        title = "",
    }})
    for _, block in ipairs(doc.blocks) do
        if(block.t == "Header" ) then
            if (block.level <= split_level) then
                curr_chapter_doc = pandoc.Pandoc{}
                chapters:insert({
                    doc = curr_chapter_doc,
                    level = block.level,
                    title = pandoc.utils.stringify(block),
                })
                block.level = 1
            else
                block.level = block.level - split_level + 1
            end
        end
        curr_chapter_doc.blocks:insert(block)
    end
    return chapters
end

local function add_chapter_urls(chapters)
    local used_ids = {}
    for _, chapter in ipairs(chapters) do
        chapter.url = unique_id(used_ids, chapter.title)
    end
end


local function normalize_header_identifiers(chapter)
    local used_ids = {}
    local function Header(header)
        header.identifier = unique_id(used_ids, pandoc.utils.stringify(header))
        return header
    end
    chapter.doc = chapter.doc:walk({Header = Header})
end

local function add_bullet_list_entry(toc, levels, block_level, block)
    -- Count and keep only the parents of the added block
    local depth = #levels:filter(function(level) return level < block_level end)
    while(#levels > depth) do
        levels:remove()
    end
    levels:insert(block_level)

    -- Find the BulletList at the right depth
    local bullet_list = toc
    for _ = 1, depth do
        local blocks = bullet_list.content:at(-1)
        if(blocks:at(-1).tag == "BulletList") then
            bullet_list = blocks:at(-1)
        else
            bullet_list = pandoc.BulletList({})
            blocks:insert(bullet_list)
        end
    end
    bullet_list.content:insert(pandoc.Blocks(block))
end

local function create_toc(chapters)
    local toc = pandoc.BulletList({});
    local levels = pandoc.List({});
    -- Ignore chapters with no level
    chapters = chapters:filter(function(chapter) return chapter.level end)
    for _, chapter in ipairs(chapters) do
        normalize_header_identifiers(chapter)
        chapter.doc:walk({
            Header = function(header)
                local level = chapter.level
                local url = chapter.url
                local class = "toc_chapter_link"
                if(header.level > 1) then
                    level = level + 100 + header.level
                    url = url.."#"..header.identifier
                    class = "toc_subchapter_link"
                end
                local link = pandoc.Link(header.content:clone(), url, nil, {class = class})
                add_bullet_list_entry(toc, levels, level, link)
            end
        })
    end
    return toc
end

local function create_toc_chapter(chapters, title, description)
    -- Exclude the TOC page from the TOC
    local chapter = chapters:remove(1)
    if(chapter.title == "") then
        chapter.title = "Spis treści"
    end
    local blocks = pandoc.Blocks({})
    blocks:insert(pandoc.Header(1, title))
    chapter.doc:walk({ Image = function(image) blocks:insert(image) end })
    blocks:insert(pandoc.Para(description))
    blocks:insert(create_toc(chapters))
    chapter.doc = pandoc.Pandoc(blocks)
    chapters:insert(1, chapter)
end

local function add_nav_relations(chapters)
    for idx, chapter in ipairs(chapters) do
        for i = idx-1, 1, -1 do
            local parent = chapters[i]
            if parent.level < chapter.level then
                chapter.parent = parent.title
                chapter.grand_parent = parent.parent
                parent.has_children = true
                break
            end
        end
    end
end

local function add_back_forward_links(chapters)
    for idx = 1, #chapters do
        local nav = pandoc.Div({}, {class = "chapter-navigator-wrapper"})
        if(idx > 1) then
            local chapter = chapters[idx - 1]
            local url = chapter.url
            if(idx == 2) then
                url = "."
            end
            local link = pandoc.Link("← "..chapter.title, url)
            link.classes:insert("chapter-navigator-prev")
            nav.content:insert(link)
        end
        if(idx < #chapters) then
            local chapter = chapters[idx + 1]
            local link = pandoc.Link(chapter.title.." →", chapter.url)
            link.classes:insert("chapter-navigator-next")
            nav.content:insert(link)
        end
        local blocks = chapters[idx].doc.blocks
        blocks:insert(nav:clone())
        blocks:insert(1, nav)
    end
end

local function add_anchors(chapters)
    for _, chapter in ipairs(chapters) do
        normalize_header_identifiers(chapter)
        local used_ids = {}
        local section_id = chapter.title

        local function Header(header)
            used_ids = {}
            section_id = header.identifier
        end

        local function Para(para)
            -- Do not add anchors on empty paragraphs
            if(not string.find(pandoc.utils.stringify(para), "%S")) then return end
            local para_id = unique_id(used_ids, section_id, true, "akapit")
            return {
                pandoc.Div({}, pandoc.Attr(para_id, { "paragraph-anchor" })),
                pandoc.Div(para, pandoc.Attr("", { "paragraph" })),
            }
        end

        chapter.doc = chapter.doc:walk({Header = Header, Para = Para})
    end
end

local function wrap_block_images_in_figures(block)
    local blocks = pandoc.List()
    local block_idx
    block = block:walk({
        traverse = 'topdown',
        Image = function(image)
            blocks:insert(pandoc.Figure(pandoc.Para(image), {long = image.caption:walk()}))
            return {}
        end,
        Inline = function(inline)
            -- This function decides if the images will be inserted before or after the block.
            -- If there's any visible text before the image, it should be placed after the block.
            if(block_idx) then return end
            -- If there's an image inside the inline, it's impossible to tell
            -- if there's any visible text before it and a deeper inspection is needed.
            local contains_image = false
            inline:walk({Image = function() contains_image = true end})
            if(contains_image) then return end
            -- If there's no image, there's no need to go deeper after text visibility check.
            if(string.match(pandoc.utils.stringify(inline), "%S")) then
                block_idx = #blocks + 1
            end
            return nil, false
        end
    })
    blocks:insert(block_idx or (#blocks + 1), block)
    return blocks
end

local function wrap_images_in_figures(chapters)
    for _, chapter in ipairs(chapters) do
        chapter.doc = chapter.doc
            -- Figures occur only in captioned images in Markdown.
            -- Unpack them to avoid nesting of figures.
            :walk({ Figure = function(figure) return figure.content end })
            :walk({
                traverse = 'topdown',
                Block = function(block) return wrap_block_images_in_figures(block), false end
            })
    end
end

local function make_image_responsive(image, used_ids, chapter, images_count, base_url, url)
    local image_id = unique_id(used_ids, chapter.title, images_count > 1)

    local images_path = pandoc.path.join({"assets", "images", url})
    local images_dir = out_path(url, images_path)
    pandoc.system.make_directory(images_dir, true)
    local images_url = pandoc.path.join({pandoc.path.separator, base_url, images_path})

    local orig_image_name = image_id..(({pandoc.path.split_extension(image.src)})[2])
    local orig_image_file = pandoc.path.join({images_dir, orig_image_name})
    local orig_image_url = pandoc.path.join({images_url, orig_image_name})

    local _, orig_image_data = pandoc.mediabag.fetch(image.src)
    io.open(orig_image_file, "wb"):write(orig_image_data):close()

    local target_width_px = 736
    if (image.attributes.width) then
        local widthCm = tonumber(string.match(image.attributes.width, "^(%d+%.?%d*)cm$"))
        if not widthCm then
            error("Unsupported image width format: '"..image.attributes.width.."'")
        end
        target_width_px = math.min(target_width_px, math.floor(widthCm / 2.54 * 96));
    end

    local widths = pandoc.List{target_width_px, target_width_px * 2, target_width_px * 3}
    local max_width = pandoc.image.size(orig_image_data).width
    if widths:at(-1) > max_width then
        widths = widths:filter(function(width) return width * 1.1 < max_width end)
        widths:insert(max_width)
    end

    local src
    local srcset_entries = pandoc.List()
    for _, width in ipairs(widths) do
        local image_name = image_id.."-"..width..".jpg"
        local image_file = pandoc.path.join({images_dir, image_name})
        local image_url = pandoc.path.join({images_url, image_name})
        src = src or image_url
        srcset_entries:insert(image_url.." "..width.."w")
        io.open(temp_path("convert_images"), "a")
            :write(orig_image_file..";"..image_file..";"..width.."\n")
            :close()
    end

    local alt = pandoc.utils.stringify(image.caption)
    if(not string.match(alt, "%S")) then
        alt = chapter.title
    end
    alt = string.gsub(alt, '"', "&quot;")
    alt = string.gsub(alt, "&", "&amp;")

    local width = target_width_px.."px"
    local image_tag = pandoc.RawInline("html", '<img '
        ..'src="'..src..'" '
        ..'srcset="'..table.concat(srcset_entries, ", ")..'" '
        ..'alt="'..alt..'" '
        ..'style="width:'..width..'" '
        ..'sizes="(min-width: 50rem) min('..width..', calc(100vw - 20rem)), '
        ..'min('..width..', calc(100vw - 2rem))">')
    return {pandoc.Link(image_tag, orig_image_url, nil, {class = "image_link", target = "_blank"})}
end

local function make_images_responsive(chapters, base_url, url)
    local used_ids = {}
    for _, chapter in ipairs(chapters) do
        images_count = 0
        chapter.doc = chapter.doc
            :walk({ Image = function() images_count = images_count + 1 end })
            :walk({ Image = function(image)
                return make_image_responsive(image, used_ids, chapter, images_count, base_url, url)
            end })
    end
end

local function write_html(chapters, url)
    for idx, chapter in ipairs(chapters) do
        local file = io.open(out_path(url, idx..".html"), "w")
        file:write("---\n")
        file:write("title: '"..chapter.title.."'\n")
        file:write("permalink: "..url.."/"..chapter.url.."\n")
        file:write("nav_order: "..idx.."\n")
        if(chapter.nav_exclude) then
            file:write("nav_exclude: true\n")
        end
        file:write("layout: default\n")
        file:write("has_toc: false\n")
        file:write("image: /assets/images/logo-seo.jpg\n")
        if(chapter.parent) then
            file:write("parent: '"..chapter.parent.."'\n")
        end
        if(chapter.grand_parent) then
            file:write("grand_parent: '"..chapter.grand_parent.."'\n")
        end
        if(chapter.has_children) then
            file:write("has_children: true\n")
        end
        file:write("---\n")
        file:write(pandoc.write(chapter.doc, "html"))
        file:close()
    end
end

local function getenv(name)
    return os.getenv(name) or error("'"..name.."' environment variable not set")
end

local function write_config(base_url, url, title, description)
    pandoc.system.make_directory(out_path(url), true)
    local file = io.open(out_path(url, "_config.yml"), "w")
    file:write("baseurl: '"..base_url.."'\n")
    file:write("url: '"..getenv("URL").."'\n")
    file:write("title: '"..title.."'\n")
    file:write("description: '"..description.."'\n")
    file:write("repo_url: '"..getenv("REPO_URL").."'\n")
    file:write("typesense_url: '"..getenv("TYPESENSE_URL").."'\n")
    file:write("author: 'Leszek Żuk'\n")
    file:write("theme: just-the-docs\n")
    file:write("logo: /assets/images/logo.png\n")
    file:write("lang: 'pl-PL'\n")
    file:write("search_enabled: false\n")
    file:write("webmaster_verifications:\n")
    file:write("  google: uMmugKDiftKRY4HkcuTdcLI0QGuqbpfcDD4qBEfni-U\n")
    file:close()
end

local book = {}

local books_list_path = temp_path("books_list.json")

function book.read_doc(path)
    local _, content = pandoc.mediabag.fetch(pandoc.path.join({"inputs", path}))
    return pandoc.read(content, pandoc.format.from_path(path))
end

function book.read_book_infos()
    local books_file = io.open(books_list_path, "r")
    if(not books_file) then
        return {}
    end
    return pandoc.json.decode(books_file:read("a"))
end

local function write_book_info(url, title, description)
    local books = book.read_book_infos()
    table.insert(books, {url = url, title = title, description = description})
    io.open(books_list_path, "w")
        :write(pandoc.json.encode(books))
        :close()
end

function book.create_chapters(doc)
    doc = doc:walk(clean_up_headers)
    local chapters = split_chapters(doc)
    add_chapter_urls(chapters)
    return chapters
end

function book.add_book_navigation(chapters, title, description)
    create_toc_chapter(chapters, title, description)
    add_nav_relations(chapters)
    add_back_forward_links(chapters)
end

function book.write_chapters(chapters, url, title, description)
    local base_url = getenv("BASEURL");
    pandoc.system.make_directory(out_path(url), true)
    add_anchors(chapters)
    wrap_images_in_figures(chapters)
    make_images_responsive(chapters, base_url, url)
    write_book_info(url, title, description)
    write_html(chapters, url);
    write_config(base_url, url, title, description)
end

function book.write(doc, url, title, description)
    local chapters = book.create_chapters(doc)
    book.add_book_navigation(chapters, title, description)
    book.write_chapters(chapters, url, title, description)
end

return book
