local filters = require("script.helpers.convert_books.filters")
local book = require("script.helpers.convert_books.book")

local fix_header = {}

function fix_header.Header(header)
    if(header.level == 3) then
        header.level = 4
    end
    return filters.gsub_header(header, 1, "^%d. ", "")
end

return function()
    doc = book.read_doc("SkądPrzychodzi.odt")
        :walk(filters.remove_anchors)
        :walk(filters.remove_block_quotes)
        :walk(fix_header)
    book.write(doc, "skad", "Skąd przychodzimy, dokąd zmierzamy",
        "Książka pokazuje wielowymiarowość ludzkiej istoty postrzeganej w "
        .."perspektywie kosmicznej i planetarnej. W przystępnej formie prezentuje "
        .."ewolucję człowieka, zagadnienia kultury, świadomości i życia duchowego.")
end
