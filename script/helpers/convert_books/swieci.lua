local filters = require("script.helpers.convert_books.filters")
local book = require("script.helpers.convert_books.book")

local remove_link = {}

function remove_link.Link(link)
    -- Replace the link with the plain text with all formatting cleared
    return { pandoc.utils.stringify(link) }
end

local fix_header = {}

function fix_header.Header(header)
    local pattern = "^[%d.]+ "
    if(header.level == 3 and not pandoc.utils.stringify(header.content):find(pattern)) then
        header.level = 4
    end
    filters.gsub_header(header, 3, pattern, "")
    filters.gsub_header(header, 2, pattern, "")
    return header
end

return function()
    doc = book.read_doc("01chrzescij.odt")
        :walk(filters.remove_anchors)
        :walk(filters.remove_block_quotes)
        :walk(filters.convert_fake_lists_to_lists)
        :walk(remove_link)
        :walk(fix_header)
    book.write(doc, "swieci", "Święty na Każdy Dzień",
        "Prezentacja pojęcia świętości i postaw religijnych zależnych od indywidualnych cech "
        .."człowieka, jego pochodzenia, wykształcenia i sposobu myślenia. Zawiera zwięzłe "
        .."charakterystyki popularnych świętych z podziałem na kościoły i odłamy chrześcijaństwa.")
end
