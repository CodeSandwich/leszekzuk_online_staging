local filters = require("script.helpers.convert_books.filters")
local book = require("script.helpers.convert_books.book")

local fix_header = {}

function fix_header.Header(header)
    if(header.level == 7) then
        header.level = 4
    end
    return filters.gsub_header(header, 1, "Rozdział %d+. ", "")
end

function create_index_links(chapters)
    local filter = {Para = function(para)
        local content = pandoc.utils.stringify(para):gsub(" %- wstęp.$", " - rozdział 0.")
        local topic, refs = content:match("^(.*) %- rozdział ([%d,]+)%.$")
        if(not topic) then
            return {pandoc.Header(3, content)};
        end
        local links = pandoc.List({})
        for ref in refs:gmatch("%d+") do
            local chapter = chapters[ref + 2]
            links:insert(pandoc.Link(chapter.title, chapter.url))
        end
        return {pandoc.Para(topic), pandoc.BulletList(links)};
    end}
    chapters:at(-2).doc = chapters:at(-2).doc:walk(filter)
    chapters:at(-1).doc = chapters:at(-1).doc:walk(filter)
end

return function()
    doc = book.read_doc("00ARCHEO.odt")
        :walk(filters.remove_anchors)
        :walk(filters.remove_block_quotes)
        :walk(fix_header)
    local chapters = book.create_chapters(doc)
    local title = "Awanturnicy, fantaści i uczeni"
    local description = "Dzieje archeologii, najsłynniejszych odkryć i głównych idei kształtujących"
        .." tę dziedzinę wiedzy przez pryzmat ludzi, którzy tego dokonali. To opis ewolucji obrazu"
        .." naszej przeszłości od prehistorii po odkrywanie Antarktydy i II wojnę światową."
    book.add_book_navigation(chapters, title, description)
    create_index_links(chapters)
    book.write_chapters(chapters, "archeologia", title, description)
end
