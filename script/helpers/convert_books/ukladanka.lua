local filters = require("script.helpers.convert_books.filters")
local book = require("script.helpers.convert_books.book")

-- For each block iterates over all paragraphs and merges them.
-- The merged paragraphs are joined with line breaks to keep them separate lines.
-- Any empty paragraph is considered a separator, it's discarded and a new paragraph is opened.
local merge_paragraphs = {}

function merge_paragraphs.Blocks(blocks)
    -- The paragraph currently being extended with consecutive paragraphs' contents.
    local para
    local new_blocks = pandoc.Blocks{}
    for _, block in ipairs(blocks) do
        if block.t ~= "Para" then -- Not a paragraph.
            para = nil
            new_blocks:insert(block)
        elseif #block.content == 0 then -- An empty paragraph.
            para = nil
        elseif para == nil then -- The first non-empty paragraph to be joined.
            para = block
            new_blocks:insert(block)
        else -- A non-first non-empty paragraph to be joined.
            para.content:insert(pandoc.LineBreak())
            para.content:extend(block.content)
        end
    end
    return new_blocks
end

return function()
    doc = book.read_doc("układanka.odt")
        :walk(filters.remove_block_quotes)
        :walk(merge_paragraphs)
    book.write(doc, "ukladanka", "Układanka",
        "Układanka jest poetycko-intelektualną zabawą, podczas której odkrywamy, że nasze pytania "
        .."o sens zawsze prowadzą do niejednoznacznych i paradoksalnych odpowiedzi, które niewiele "
        .."wyjaśniają, lecz rodzą kolejne pytania. A jednak zauważamy, że warto je stawiać.")
end
