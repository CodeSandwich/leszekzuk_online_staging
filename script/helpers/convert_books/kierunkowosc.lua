local filters = require("script.helpers.convert_books.filters")
local book = require("script.helpers.convert_books.book")

local create_headers = {}

function create_headers.Para(para)
    local stringified = pandoc.utils.stringify(para)
    -- Create the book title header and trim the paper header
    if(string.match(stringified, "^One of the fundamental problems")) then
        return { pandoc.Header(1, "Kierunkowość procesów ewolucji") }
    -- Create the references header
    elseif(stringified == "Cytowana literatura") then
        return { pandoc.Header(4, stringified) }
    end
end

return function()
    doc = book.read_doc("kierunkowośćEwol.odt")
        :walk(filters.remove_block_quotes)
        :walk(create_headers)
    -- Make the first regular chapter the only one
    local chapter = book.create_chapters(doc)[2]
    chapter.url = ""
    chapter.nav_exclude = true
    book.write_chapters(pandoc.List({chapter}), "kierunkowosc", chapter.title,
        "Tekst pokazuje, ze zauważalne ukierunkowanie ewolucji zarówno w biologii jak też "
        .."w innych obszarach rzeczywistości nie jest celowe czyli nie zostało zaplanowane "
        .."na osiągnięcie określonego stanu. Ukierunkowanie wynika z samej natury świata.")
end
