local book = require("script.helpers.convert_books.book")

return function()
    local doc = book.read_doc("test.md")
    book.write(doc, "test", "Jekyll Test Page", "This is a Jekyll test page (150 - 250 chars)")
end
