local book = require("script.helpers.convert_books.book")

return function()
    book.write(book.read_doc("PersonalizmMed.odt"), "personalizm",
        "Wybrane aspekty etyki medycznej w ujęciu personalizmu chrześcijańskiego",
        "Personalizm chrześcijański w centrum uwagi nie stawia osoby "
        .."człowieka lecz postrzega człowieka wyłącznie w relacji "
        .."do Boga, co jest właściwie zaprzeczeniem personalizmu.")
end
