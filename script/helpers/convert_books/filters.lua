local filters = {}

-- Runs gsub on the header content if it has the matching level.
function filters.gsub_header(header, level, pattern, repl)
    if(header.level ~= level) then return end
    header.content = pandoc.utils.stringify(header.content):gsub(pattern, repl);
    header.identifier = ""
    return header
end

filters.convert_fake_lists_to_lists = {}

function filters.convert_fake_lists_to_lists.Blocks(blocks)
    -- The paragraph currently being extended with consecutive paragraphs' contents.
    local bullet_list
    local new_blocks = pandoc.List()
    for _, block in ipairs(blocks) do
        local content = block.t == "Para" and pandoc.utils.stringify(block)
        if content == "-" then -- Empty fake list item, ignore.
        elseif not content or not string.find(content, "^- ") then -- Not a fake list item.
            bullet_list = nil
            new_blocks:insert(block)
        else -- A fake list item.
            -- Remove the first instance of string "-" and the first instance of a space
            local str_found, space_found
            block = block:walk {
                Str = function ()
                    if not str_found then
                        str_found = true
                        return {}
                    end
                end,
                Space = function ()
                    if not space_found then
                        space_found = true
                        return {}
                    end
                end
            }
            if not bullet_list then -- The first fake list item.
                bullet_list = pandoc.BulletList(block)
                new_blocks:insert(bullet_list)
            else -- A non-first fake list item.
                bullet_list.content:insert(block)
            end
        end
    end
    return new_blocks
end

-- Removes the anchors.
-- They are empty spans with an identifier `anchor` sometimes followed by a numeric suffix.
-- These anchors are passed into the output pulluting it.
filters.remove_anchors = {}

function filters.remove_anchors.Span(span)
    if(span.identifier:match("^anchor") and #span.content == 0) then return {} end
end

-- Un-quotes all text by replacing each `BlockQuote` with its content.
-- Pandoc detects block quotes in ODT based on the indentation of the text,
-- which often creates many false positives.
filters.remove_block_quotes = {}

function filters.remove_block_quotes.BlockQuote(quote)
    return quote.content
end

return filters
