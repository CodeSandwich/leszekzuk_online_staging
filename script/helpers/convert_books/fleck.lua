local filters = require("script.helpers.convert_books.filters")
local book = require("script.helpers.convert_books.book")

local fix_header = {}

function fix_header.Header(header)
    return filters.gsub_header(header, 1, "^[IV]+$", "Rozdział %0")
end

return function()
    doc = book.read_doc("Fleck.odt")
        :walk(filters.remove_block_quotes)
        :walk(fix_header)
    book.write(doc, "fleck", "Trzy aspekty poznania naukowego w świetle koncepcji Ludwika Flecka",
        "Znany polsko-żydowski lekarz i filozof poznania pokazuje, że nauka tylko "
        .."w pewnym stopniu jest obiektywnym sposobem poznawania rzeczywistości. "
        .."Przede wszystkim zaś jest społeczną konwencją.")
end
