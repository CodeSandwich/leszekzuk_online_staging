local filters = require("script.helpers.convert_books.filters")
local book = require("script.helpers.convert_books.book")

local fix_header = {}

function fix_header.Header(header)
    return filters.gsub_header(header, 1, "^", "Rozdział ")
end

return function()
    doc = book.read_doc("Kaczy folwark.odt")
        :walk(fix_header)
    book.write(doc, "folwark", "Kaczy folwark: baśń ze świata zwierząt",
        "Folwark Zwierzęcy to groteskowy obraz świata, gdzie idolem jest mityczna wola "
        .."większości. Widać, jak łatwo mechanizmy demokratyczne można wykoślawić aż do "
        .."autorytaryzmu w imię pieniędzy, awansu czy zemsty za własne niepowodzenia.")
end
