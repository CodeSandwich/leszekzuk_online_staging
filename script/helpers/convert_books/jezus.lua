local book = require("script.helpers.convert_books.book")

local create_headers = {}

function create_headers.Para(para)
    local chapter
    local text = pandoc.utils.stringify(para)
    if(text == "Literatura") then
        chapter = text
    else
        chapter = string.match(text, "^%d%. (.+)$")
    end
    if(chapter) then
        return { pandoc.Header(1, chapter) }
    end
end

local notes = {}

local collect_notes = {}

function collect_notes.Para(para)
    if(para.content[1].t ~= "Superscript") then
        return
    end
    local key = pandoc.utils.stringify(para.content:remove(1))
    para.content:remove(1)
    notes[key] = para
    return {}
end

local create_notes = {}

function create_notes.Superscript(superscript)
    local text = pandoc.utils.stringify(superscript)
    local inlines = pandoc.List()
    for key, separator in string.gmatch(text, "(%d+)([^%d]*)") do
        inlines:insert(pandoc.Note(notes[key]))
        inlines:insert(pandoc.Superscript(separator))
    end
    return inlines
end

return function()
    doc = book.read_doc("Jezusamob.docx")
        :walk(create_headers)
        :walk(collect_notes)
        :walk(create_notes)
    book.write(doc, "jezus", "Czy Jezus popełnił samobójstwo?",
        "Ewangelie można uznać za opis zaplanowanego samobójstwa, co od dawna wywołuje kontrowersje"
        .." wokół stanu psychiki Jezusa. Anomalne, choć niekoniecznie patologiczne cechy "
        .."Jezusa zdecydowały, że prowincjonalny wizjoner został uznany mesjaszem lub bóstwem.")
end
