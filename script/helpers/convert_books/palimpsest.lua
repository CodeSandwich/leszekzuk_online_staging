local filters = require("script.helpers.convert_books.filters")
local book = require("script.helpers.convert_books.book")

local clearEmph = {}

function clearEmph.Emph(emph)
    return emph.content
end

local clearStrong = {}

function clearStrong.Strong(strong)
    return strong.content
end

local createHeaders = {}

function createHeaders.Para(para)
    local chapter, sub = string.match(pandoc.utils.stringify(para), "^ *([soiu][.,]%d%d[.,](%d?%d?%.?)) *$")
    if(not chapter) then
        return
    elseif(sub == "") then
        level = 2
    else
        level = 3
    end
    return { pandoc.Header(level, string.gsub(chapter, ",", "."):upper()) }
end

local createLinks = {}

function createLinks.Para(para)
    -- Remove the arrows before the links
    local text = string.gsub(pandoc.utils.stringify(para), " ?→ ?", " ")
    local before, chapter, after = string.match(text, "(.*)([soiu][.,]%d%d[.,]%d?%d?%.?)(.*)")
    if(not chapter) then
        return para
    end
    chapter = string.gsub(chapter, ",", "."):upper()
    local chapterPath = ({
        S = "subiectivum",
        O = "obiectivum",
        I = "intersubiectivum",
        U = "universum"
    })[pandoc.text.sub(chapter, 1, 1)]
    local sectionPath = string.gsub(string.gsub(chapter, "%.", "-"), "%-$", ""):lower()
    local link = pandoc.Link(chapter, chapterPath.."#"..sectionPath)

    local newPara = createLinks.Para(pandoc.Para({before}))
    newPara.content:insert(link)
    newPara.content:extend(pandoc.Para({after}).content)
    return newPara
end

return function()
    doc = book.read_doc("Palimpsest.odt")
        :walk(filters.remove_anchors)
        :walk(filters.remove_block_quotes)
        :walk(clearEmph)
        :walk(clearStrong)
    local chapters = book.create_chapters(doc)
    local title = "Palimpsest"
    local description = "Brat Seraphinos poświęcił życie, żeby zrozumieć świat. "
        .."Szukając czegoś na kształt kamienia filozoficznego odkrył, że wszystkie "
        .."sposoby poznania są równorzędne i wzajemnie się uzupełniają. "
        .."Tak powstał swoisty przegląd kierunków filozoficznych Palimpsest."
    book.add_book_navigation(chapters, title, description)
    -- Create headers after TOC is generated to avoid including them
    for _, chapter in ipairs(chapters) do
        chapter.doc = chapter.doc
            :walk(createHeaders)
            :walk(createLinks)
    end
    book.write_chapters(chapters, "palimpsest", title, description)
end
