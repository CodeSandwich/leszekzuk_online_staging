set -e

IMAGES=temp/convert_images
export IMAGES_COUNT=$(wc -l < "$IMAGES")
# Add line numbers and make arguments null-separated
sed = "$IMAGES" | tr ';\n' '\0' | xargs -P $(nproc) -0 -n 4 sh -c '
    echo "Converting image $0/$IMAGES_COUNT: $2"
    MAGICK_THREAD_LIMIT=1 \
        magick "$1" -alpha remove -resize "$3" -strip -interlace plane -quality 80 "$2"
'
