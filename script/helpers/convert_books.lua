local function convert_book(name)
    print("Converting "..name)
    require("script.helpers.convert_books."..name)()
end

pcall(function() pandoc.system.remove_directory("temp", true) end)

if(os.getenv("TEST") == "1") then
    convert_book("test")
end
convert_book("czlowiek")
convert_book("archeologia")
convert_book("swieci")
convert_book("jezus")
convert_book("folwark")
convert_book("fleck")
convert_book("kierunkowosc")
convert_book("personalizm")
convert_book("skad")
convert_book("ukladanka")
convert_book("palimpsest")
convert_book("site")
